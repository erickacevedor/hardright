(function($) {

	/* Nav */ 
	var heroHeightB = 1;

	$(window).scroll(function(event){
		var st = $(this).scrollTop();

		if(st >= heroHeightB){
			$("body").addClass("no-top");
			$("body").removeClass("is-top");
		}else{
			$("body").removeClass("no-top");
			$("body").addClass("is-top");
		}
	});




	$('#main-nav-button').on("click", function () {
		$('body').toggleClass("open-menu-mobile");
	});


	$('.timer').countTo();

	$("#modal-video").animatedModal({
		animatedIn:'fadeIn',
		animatedOut:'fadeOut',
		animationDuration: '.7s'
	});

	$('.js-example-basic-single').select2();
	$('.owl-carousel').owlCarousel({
		loop:false,
		margin: 0,
		navText:['<img class="arrow-items" src="dist/img/arrow-left.svg"/>', '<img class="arrow-items" src="dist/img/arrow-right.svg"/>'],
		nav:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			1000:{
				items:3
			}
		}
	})

	$("#contact-us").validate({
		rules: {
			name : {
				required: true,
				minlength: 3
			},
			email: {
				required: true,
				email: true
			},
			phone: {
				required: true,
		        number: true,
		        min: 18
			},
			respond: {
				required: true,
			},
			message: {
				required: true,
			},
		},
		messages : {
			name: {
				required: "This field is required"
			},
			age: {
				required: "This field is required"
			},
			email: {
				required: "This field is required"
			},
			phone: {
				required: "This field is required"
			},
			respond: {
				required: "This field is required"
			},
			message: {
				required: "This field is required"
			}
		}
	});

})( jQuery );





