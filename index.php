<!doctype html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">


  <title>Hardright</title>
  <meta name="description" content="HR software that really matters">

  <link rel="apple-touch-icon" sizes="180x180" href="dist/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="dist/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="dist/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="dist/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="dist/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <?php include 'versionado.php'; ?>

  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="dist/css/style.css<?=$ver;?>">
  <link rel="stylesheet" type="text/css" href="build/libs/font-awesome-4.6.3/css/font-awesome.min.css">
  <link rel="stylesheet" href="dist/libs/OwlCarousel2-2.3.4/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.0/animate.min.css">


</head>


<body class="home">
  <div id="animatedModal">
    <div class="close-animatedModal">
      <span class="close">&times;</span>
    </div>
    <div class="modal-content">
      <div class="embed-responsive embed-responsive-16by9">
        <div id="player"></div>
      </div>
    </div>
  </div>


  <nav class="main-nav">
    <div class="container">
      <div class="logo"></div>
      <div class="menu-container">

        <ul class="main-nav-items">
          <li class="main-menu-nav-item"><a href="#">Products</a></li>
          <li class="main-menu-nav-item"><a href="#">Pricing</a></li>
          <li class="main-menu-nav-item"><a href="#">Solutions</a></li>
          <li class="main-menu-nav-item"><a href="#">Demo</a></li>
          <li class="main-menu-nav-item"><a href="#">Services</a></li>
          <li class="main-menu-nav-item main-menu-nav-item-last"><a href="#">Get started</a></li>
        </ul>

      </div>

      <button id="main-nav-button" class="main-nav-button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      
    </div>
  </nav>


  <div class="hero box nostatic">
    <div class="container">
      <div class="hero-text box">
        <h1><span>HR software</span>that really matters.</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto delectus suscipit fugiat, officia aliquam placeat qui aperiam eaque quos fugit minus temporibus incidunt harum iusto perferendis voluptates quae quaerat tempora.
        </p>
        <a class="btn btn-blue" href="#">Get started</a>
      </div>
    </div>
  </div>

  <!-- 2 -->
  <section class="getahead page-section box row-va">
    <div class="getahead-image">
      <img src="dist/img/getahead-image.jpg" alt="Get ahead of the curve">
    </div>
    <div class="getahead-text">
      <div class="box">
        <h2>Get ahead of the curve</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum tenetur officiis laudantium eum, eaque veniam veritatis, cupiditate delectus distinctio magnam amet repudiandae provident nemo numquam maxime necessitatibus a laborum commodi.
        </p>
      </div>
    </div>
  </section>

  <!-- Data -->
  <section class="data page-section box">
    <div class="container">
      <div class="box data-title">
        <h5 class="">Based on data from:</h5>
      </div>
      <div class="data-items box">
        <div class="data-item data-item-a">
          <div class="data-item-content box">
            <img src="dist/img/Icon_TICKETS.svg" alt="Tickets">
            <div>
              <h4 class="timer" data-from="0" data-to="200"></h4>
              <span>Tickets</span>
            </div>
          </div>
        </div>
        <div class="data-item data-item-b">
          <div class="data-item-content box">
            <img src="dist/img/Icon_CUSTOMERS.svg" alt="Customers">
            <div>
              <h4 class="timer" data-from="0" data-to="300"></h4>
              <span>Customers</span>
            </div>

          </div>
        </div>
        <div class="data-item data-item-c">
          <div class="data-item-content box">
            <img src="dist/img/icon_AGENTS.svg" alt="Agents">
            <div>
              <h4 class="timer" data-from="0" data-to="320"></h4>
              <span>Agents</span>
            </div>
          </div>
        </div>
        <div class="data-item data-item-d">
          <div class="data-item-content box">
            <img src="dist/img/icon_LAST.svg" alt="Customers">
            <div>
              <h4 class="timer" data-from="0" data-to="320"></h4>
              <span>Customers</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <!-- See ya' -->
  <section class="seewhere page-section box grey-bg text-center">
    <div class="container">
      <h2>See where you stand</h2>
      <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A laborum et officiis doloribus harum.
      </p>
      <p class="seewhere-help-text">Industry</p>
      <div class="dropdown-container">
        <span class="seewhere-icon"></span>
        <select class="js-example-basic-single" name="state">
          <option value="">Select</option>
          <option value="Education">Education</option>
          <option value="Energy">Energy</option>
          <option value="Entertainment &amp; Gaming">Entertainment &amp; Gaming</option>
          <option value="Financial Services">Financial Services</option>
          <option value="Government & Non-profit">Government & Non-profit</option>
          <option value="Healthcare">Healthcare</option>
        </select>
      </div>

    </div>
  </section>

  <!-- Learn -->
  <section class="page-section box learn">
    <div class="container">
      <div class="learn-maintext text-center">
        <h2>Learn from our research</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus nesciunt obcaecati totam dolorem repudiandae nostrum praesentium quam aspernatur non.
        </p>
      </div>
      <div class="learn-blocks box">

        <div class="owl-carousel owl-theme">


          <div class="learn-blocks-item box">
            <div class="card">
              <div class="box">
                <h4>Learn from our research</h4>
                <p>Lorem ipsum dolor</p>
              </div>
              <div class="box">
                <img src="dist/img/learn-img-1.svg" alt="Learn from our research">
              </div>
              <div class="box">
                <a href="#">Read report</a>
              </div>
            </div>
          </div>

          <div class="learn-blocks-item box">
            <div class="card">
              <div class="box">
                <h4>Learn from our research</h4>
                <p>Lorem ipsum dolor</p>
              </div>
              <div class="box">
                <img src="dist/img/learn-img-2.svg" alt="Learn from our research">
              </div>
              <div class="box">
                <a href="#">Read report</a>
              </div>

            </div>        
          </div>
          <div class="learn-blocks-item box">
            <div class="card">
              <div class="box">
                <h4>Learn from our research</h4>
                <p>Lorem ipsum dolor</p>
              </div>
              <div class="box">
                <img src="dist/img/learn-img-3.svg" alt="Learn from our research">
              </div>
              <div class="box">
                <a href="#">Read report</a>
              </div>
            </div>
          </div>
          <div class="learn-blocks-item box">
            <div class="card">
              <div class="box">
                <h4>Learn from our research</h4>
                <p>Lorem ipsum dolor</p>
              </div>
              <div class="box">
                <img src="dist/img/learn-img-1.svg" alt="Learn from our research">
              </div>
              <div class="box">
                <a href="#">Read report</a>
              </div>
            </div>
          </div>
          <div class="learn-blocks-item box">
            <div class="card">
              <div class="box">
                <h4>Learn from our research</h4>
                <p>Lorem ipsum dolor</p>
              </div>
              <div class="box">
                <img src="dist/img/learn-img-2.svg" alt="Learn from our research">
              </div>
              <div class="box">
                <a href="#">Read report</a>
              </div>

            </div>        
          </div>
          <div class="learn-blocks-item box">
            <div class="card">
              <div class="box">
                <h4>Learn from our research</h4>
                <p>Lorem ipsum dolor</p>
              </div>
              <div class="box">
                <img src="dist/img/learn-img-3.svg" alt="Learn from our research">
              </div>
              <div class="box">
                <a href="#">Read report</a>
              </div>
            </div>
          </div>

        </div>


      </div>
    </div>
  </section>


  <!-- See ya the comeback -->
  <section class="seewhere-video page-section text-center box">
    <div class="container">
      <div class="seewhere-video-maintext">
        <h2>See where you stand</h2>
        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A laborum et officiis doloribus harum.
        </p>
      </div>
      <div class="seewhere-video-media">
        <a id="modal-video" href="#animatedModal">
          <img src="dist/img/video-placeholder.jpg" alt="Video">
        </a>
      </div>

    </div>
  </section>

  <!-- Map -->
  <section class="contact-map page-section box">
    <div id="map"></div>
    <div class="contact-map-card text-center">
      <h2>Contact Us</h2>
      <p>Send us a message</p>
      <form id="contact-us" action="">


        <div class="form-group">
          <input name="name" id="name" type="text" class="inputText" required/>
          <span class="floating-label">Name</span>
        </div>
        <div class="form-group">
          <input name="email" id="email" type="text" class="inputText" required/>
          <span class="floating-label">Email</span>
        </div>
        <div class="form-group">
          <input name="phone" id="phone" type="text" class="inputText" required/>
          <span class="floating-label">Phone</span>
        </div>

        <div class="form-group">
          <label for="respond">How would you like us to respond?</label>
          <div class="form-group-radius">
            <div><input type="radio" name="respond" value="Email"> Email<br></div>
            <div><input type="radio" name="respond" value="Phone"> Phone</div>
          </div>
        </div>

        <div class="form-group">
          <label for="message">Message</label>
          <input name="message" id="message" type="text">
        </div>

        <div class="form-group form-group-last">
          <input class="btn btn-blue" type="submit">
        </div>
      </form>
    </div>
  </section>

  
  <footer id="footer" class="page-section grey-bg box">
    <div class="container">
      <hr class="footer-line">


      <div class="footer-content box">
        <div class="footer-links">



          <div class="footer-links-item">
            <p><strong>Products</strong></p>
            <ul>
              <li><a href="#">Enterprice</a></li>
              <li><a href="#">Developers</a></li>
              <li><a href="#">iPhone App</a></li>
            </ul>
          </div>
          <div class="footer-links-item">
            <p><strong>Pricing</strong></p>
            <ul>
              <li><a href="#">Premier Pro</a></li>
              <li><a href="#">After Effects</a></li>
              <li><a href="#">Final Cut Pro</a></li>
            </ul>
          </div>

          <div class="footer-links-item">
            <p><strong>Solutions</strong></p>
            <ul>
              <li><a href="#">Careers</a></li>
              <li><a href="#">Blog</a></li>
              <li><a href="#">Swag Store</a></li>
            </ul>
          </div>
          <div class="footer-links-item">
            <p><strong>Demo</strong></p>
            <ul>
              <li><a href="#">Support</a></li>
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">Status Page</a></li>
            </ul>
          </div>

          <div class="footer-links-item">
            <p><strong>Services</strong></p>
            <ul>
              <li><a href="#">Pricing</a></li>
              <li><a href="#">Customers</a></li>
              <li><a href="#">SLA</a></li>
            </ul>
          </div>

        </div>
        <div class="footer-login">

          <a href="#" class="btn">Login</a>
          <a href="#" class="btn btn-blue">Get Started</a>

        </div>
      </div>    



    </div>
  </footer>


  <div class="copy-container box grey-bg">
    <div class="container">
      <div class="copy">
        <div class="copy-logo-links">

          <div class="copy-links">
            <p>
              <span>HardRigth.io, Inc</span>
              <a href="#">Terms</a>
              <a href="#">Privacy</a>
            </p>

          </div>
          <div class="copy-logo">
            <img src="dist/img/logo-footer.svg" alt="HardRigth">
          </div>

        </div>

        <div class="copy-socialmedia">
          <ul>
            <li>
              <a href="#"><img src="dist/img/sm/facebook.svg" alt="Facebook"></a>
            </li>
            <li>
              <a href="#"><img src="dist/img/sm/twitter.svg" alt="Twitter"></a>
            </li>
            <li>
              <a href="#"><img src="dist/img/sm/youtube.svg" alt="Youtube"></a>
            </li>
            <li>
              <a href="#"><img src="dist/img/sm/pinterest-sign.svg" alt="Pinterest"></a>
            </li>
            <li>
              <a href="#"><img src="dist/img/sm/linkedin.svg" alt="LinkedIn"></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <script>
    var map;
    function initMap() {

      var ideaware = {lat: 11.005969, lng: -74.803040};

      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: ideaware
      });

      var contentString = '<div id="contentString" style="padding: 20px;"><h5>Fields court station road</h5><p style="margin: 0;">Epworth DN9 1JZ - United Kingdom Company Reg: 8202467</p></div>';

      var iconBase = 'dist/img/';
      var icon = {
        url: iconBase + 'map-point.svg',
        scaledSize: new google.maps.Size(50, 50),
      }

      var infowindow = new google.maps.InfoWindow({
        content: contentString
      });

      var marker = new google.maps.Marker({
        position: ideaware,
        map: map,
        icon: icon,
        title: 'Uluru (Ayers Rock)'
      });


      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });

      var styledMapType = new google.maps.StyledMapType([
      {
        "elementType": "geometry",
        "stylers": [
        {
          "color": "#f5f5f5"
        }
        ]
      },
      {
        "elementType": "labels.icon",
        "stylers": [
        {
          "visibility": "off"
        }
        ]
      },
      {
        "elementType": "labels.text.fill",
        "stylers": [
        {
          "color": "#616161"
        }
        ]
      },
      {
        "elementType": "labels.text.stroke",
        "stylers": [
        {
          "color": "#f5f5f5"
        }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
        {
          "color": "#bdbdbd"
        }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
        {
          "color": "#eeeeee"
        }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
        {
          "color": "#757575"
        }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
        {
          "color": "#e5e5e5"
        }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
        {
          "color": "#9e9e9e"
        }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
        {
          "color": "#ffffff"
        }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
        {
          "color": "#757575"
        }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
        {
          "color": "#dadada"
        }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
        {
          "color": "#616161"
        }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
        {
          "color": "#9e9e9e"
        }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
        {
          "color": "#e5e5e5"
        }
        ]
      },
      {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
        {
          "color": "#eeeeee"
        }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
        {
          "color": "#c9c9c9"
        }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
        {
          "color": "#9e9e9e"
        }
        ]
      }
      ],

      {name: 'Styled Map'});
      map.mapTypes.set('styled_map', styledMapType);
      map.setMapTypeId('styled_map');

    }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgNPdXWSxDLBFDzm0uTrEwGz_2SF7rTao&callback=initMap"
  async defer></script>
  

  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <!-- Select -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js"></script>

  <script src="dist/libs/OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
  <script src="dist/libs/jquery.countTo.js"></script>
  <script src="dist/libs/animatedModal.min.js"></script>
  <script src="dist/js/app.js<?=$ver;?>"></script>

  <script>
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var player;
    function onYouTubeIframeAPIReady() {
      player = new YT.Player('player', {
        height: '360',
        width: '640',
        videoId: '3n1T3HxHd7Y'
      });
    }
    var done = false;
    function stopVideo() {
      player.stopVideo();
    }
    (function($) {
      $(".close-animatedModal").on("click", function () {
        player.stopVideo();
        done = true;
      });
    })( jQuery );
  </script>

</body>
</html>